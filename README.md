# Minds Policies

- [Terms of Service](https://gitlab.com/minds/terms-of-service/blob/master/TERMS.md)
- [Privacy Policy](https://gitlab.com/minds/terms-of-service/blob/master/PRIVACY.md)
